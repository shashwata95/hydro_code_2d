#ifndef RIEMANN_H
#define RIEMANN_H
#include <iostream>

#include "cell.h"
#include "all_constants.h"

class Riemann
{
public:
	virtual void ComputeFlux(Cell cL, Cell cR, double *flux, double vmax){
		return;
	}
	

};
class RiemannLF: public Riemann
{
public:
	void ComputeFlux(Cell cL, Cell cR, double *flux, double vmax, int dir);
	void CellCentredFlux(Cell, double*, int);

	
};

#endif