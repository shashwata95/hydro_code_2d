#include <iostream>
#include <cmath>
#include "all_constants.h"
#include "riemann.h"

void RiemannLF::ComputeFlux(Cell cL, Cell cR, double *flux, double vmax, int dir){
	double fL[nvar], fR[nvar];
	CellCentredFlux(cL, fL, dir);
	CellCentredFlux(cR, fR, dir);
	for (int var=0; var<nvar; var++) 
		flux[var] = 0.5*(fL[var] + fR[var]) + 0.5*vmax*(cL.Qcons[var] - cR.Qcons[var]);
	// std::cout<<"flux are: ";
	// for (int var=0; var<nvar; var++)std::cout<<flux[var]<<"	";
	// std::cout<<endl;

	return;
}

void RiemannLF::CellCentredFlux(Cell cell, double *flux, int dir){ //dir=0:x, dir=1:y
	double vx = cell.vel[dir];
	flux[irho] = cell.rho*vx; //density flux
	for (int jj=0; jj<ndim; jj++) flux[jj] = cell.rho*vx*cell.vel[jj]; //velocity flux
	flux[dir] += cell.press;
	flux[iE] = gamma1/(gamma1-1.)*cell.press;
	for (int jj=0; jj<ndim; jj++) flux[iE] += 0.5*cell.rho*cell.vel[jj]*cell.vel[jj];
	flux[iE] *= vx;

	return;
}