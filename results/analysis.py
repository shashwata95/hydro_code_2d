import numpy as np 
import matplotlib
import matplotlib.pyplot as plt
x, y, vx, vy, rho, p = np.loadtxt('sedov100_00006.dat', skiprows=2, unpack=True)
x2, y2, vx2, vy2, rho2, p2 = np.loadtxt('sedov200_00006.dat', skiprows=2, unpack=True)
x5, y5, vx5, vy5, rho5, p5 = np.loadtxt('sedov500_00006.dat', skiprows=2, unpack=True)

r = np.sqrt(x*x + y*y)
r2 = np.sqrt(x2*x2 + y2*y2)
r5 = np.sqrt(x5*x5 + y5*y5)

fig, ax = plt.subplots(nrows=1, ncols=1)

ax.plot(r, p, 'b.', label='$n=100^2$')
ax.plot(r2, p2, 'r.', label='$n=200^2$')
ax.plot(r5, p5, 'g.', label='$n=500^2$')
ax.set_xlabel('r')
ax.set_ylabel('pressure')
plt.legend()
plt.show()