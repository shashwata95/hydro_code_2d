#include <iostream>
#include <cmath>
#include "solve.h"
#include "all_constants.h"

double Solve::GlobalFastWaveSpeed(Cell* cell)
{
	double fws = 0.;
	double tmp = 0.;
	double vel;
	for (int i = 0; i < ncells; i++){
		vel = 0.;
		for (int kk=0; kk<ndim; kk++) vel += cell[i].vel[kk]*cell[i].vel[kk];
		vel = sqrt(vel);
		double cs2 = gamma1*cell[i].press/cell[i].rho;
		tmp = sqrt(cs2) + vel;
		if (tmp > fws) fws = tmp;
	}
	// std::cout<<"fast wave speed: "<<fws<<endl;
	return fws;
}


void Solve::InitializeConserved(Cell* cell) //Converts primitive variables into conserved variables
{
	for (int i = 0; i < ncells; i++){
		cell[i].Qcons[irho] = cell[i].rho;
		cell[i].Qcons[iE] = cell[i].etot;
		for (int kk = 0; kk < ndim; kk++) cell[i].Qcons[kk] = cell[i].rho*cell[i].vel[kk];
		for (int var =0; var < nvar; var++) cell[i].dQ[var] = 0.;
	}

}

void Solve::CellFromQcons(Cell* cell, double dt) //Converts conserved variables into primitive variables
{
	for (int i = 0; i < ncells; i++){
		cell[i].rho = cell[i].Qcons[irho];
		cell[i].etot = cell[i].Qcons[iE];
		for (int j = 0; j < ndim; j++){
			cell[i].vel[j] = cell[i].Qcons[j]/cell[i].Qcons[irho];
		}
		double ekin = 0;
		for (int j = 0; j < ndim; j++) {
			ekin += 0.5*cell[i].rho*cell[i].vel[j]*cell[i].vel[j];
		}
		cell[i].inten = (cell[i].etot - ekin)/cell[i].rho;
		cell[i].press = (gamma1-1.)*cell[i].rho*cell[i].inten;
	}
}

void Solve::InitializeFlux(double* flux){
	for (int jj=0; jj<nvar; jj++){
		flux[jj] = 0.;
	}
	return;
}

double Solve::FastWaveSpeed(Cell cL, Cell cR){ //Calculates local lambda max
	double vL = 0.; double vR = 0.;
	for (int kk=0; kk<ndim; kk++) vL += cL.vel[kk]*cL.vel[kk];
	for (int kk=0; kk<ndim; kk++) vR += cR.vel[kk]*cR.vel[kk];
	vL = sqrt(vL);
	vR = sqrt(vR);

	double csL = gamma1*cL.press/cL.rho;
	csL  = sqrt(csL) + vL;
	double csR = gamma1*cR.press/cR.rho;
	csR  = sqrt(csR) + vR;
	return max(csL, csR);

}

void Solve::UpdateDQ(Cell &cL, Cell &cR, double *flux, double dt, double dx){
	for (int var=0; var<nvar; var++){
		cL.dQ[var] -= dt/dx*flux[var]; //flux subtracted from left cell and added to right cell
		cR.dQ[var] += dt/dx*flux[var];
	}
	return;
}

void Solve::BoundaryCheck(Cell* cell, double dt){ //closed boundary conditions, need modification for periodic

	int id;
	// std::cout<<"boundary cells: "<<endl;
	for (int i = 0; i < Nx; i++){
		for (int var=0; var< nvar; var++) cell[i].Qcons[var] = cell[i+Nx].Qcons[var]; //bottom boundary
		for (int var=0; var< nvar; var++) cell[(Ny-1)*Nx + i].Qcons[var] = cell[(Ny-2)*Nx + i].Qcons[var]; //top boundary
		// std::cout<<i<<endl;
		// std::cout<<(Ny-1)*Nx + i<<endl;
	}
	for (int j = 0; j < Ny; j++){
		for (int var=0; var< nvar; var++) cell[j*Nx].Qcons[var] = cell[j*Nx+1].Qcons[var]; //left boundary
		for (int var=0; var< nvar; var++) cell[j*Nx + Nx-1].Qcons[var] = cell[j*Nx + Nx - 2].Qcons[var]; //right boundary
		// std::cout<<j*Nx<<endl;
		// std::cout<<j*Nx + Nx - 1<<endl;
	}


	return;
}

void Solve::UpdateQcons(Cell * cell){
	for (int cl=0; cl<ncells; cl++){
		// std::cout<<"cell: "<<cl<<"	dQ: ";
		// for (int var=0; var<nvar; var++)std::cout<<cell[cl].dQ[var]<<"	";
		// std::cout<<endl;

		for (int var=0; var<nvar; var++) cell[cl].Qcons[var] += cell[cl].dQ[var];
		for (int var=0; var<nvar; var++) cell[cl].dQ[var] = 0.;

	}
	return;
}

void Solve::UpdateCellValue(Cell* cell, double dt)
{
	double xflux[nvar], yflux[nvar];
	double vxmax, vymax;
	RiemannLF riemannLF;

	for (int i=0; i < Nx-1; i++){
		for (int j=0; j<Ny-1; j++){
			int id = j*Nx + i;
			InitializeFlux(xflux);
			InitializeFlux(yflux);
			vxmax = FastWaveSpeed(cell[id], cell[id+1]);
			vymax = FastWaveSpeed(cell[id], cell[id+Nx]);
			riemannLF.ComputeFlux(cell[id], cell[id+1], xflux, vxmax,0);
			riemannLF.ComputeFlux(cell[id], cell[id+Nx], yflux, vymax,1);
			UpdateDQ(cell[id], cell[id+1], xflux, dt, dx_global);
			UpdateDQ(cell[id], cell[id+Nx], yflux, dt, dy_global);

		}
	}



	// for (int i = 0; i<ncells-1; i++){ 						//loop over all cells in the row

	// 	InitializeFlux(flux); 								//set all flux values to 0
	// 	double vmax = FastWaveSpeed(cell[i], cell[i+1]); 	//compute lambda max
	// 	riemannLF.ComputeFlux(cell[i],cell[i+1],flux,vmax,0); //compute fluxes between a pair of cells: can be improved by
	// 														//using different Riemann solver or reconstructed flux
	// 	UpdateDQ(cell[i], cell[i+1], flux, dt, dx_global);

	// }
	UpdateQcons(cell);										//Update conserved variable array
	BoundaryCheck(cell, dt);							//implements closed boundary conditions
	for (int cl=0; cl<ncells; cl++){
		// std::cout<<"cell: "<<cl<<"	Qcons: ";
		// for (int var=0; var<nvar; var++)std::cout<<cell[cl].Qcons[var]<<"	";
		// std::cout<<endl;
	}

	CellFromQcons(cell, dt);								//updates primitive variables
	return;
}
