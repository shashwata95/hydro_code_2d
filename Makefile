FILE=./main

CXX = g++ -std=c++11
CFLAGS = -I.

RM=rm -f

SRCS=main.cpp initialize.cpp solve.cpp riemann.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

main: $(OBJS)
	$(CXX) -o $(FILE) $(OBJS) 

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean:
	$(RM) $(FILE)
