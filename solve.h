#ifndef SOLVE_H
#define SOLVE_H

#include <iostream>
#include <cmath>
#include "cell.h"
#include "all_constants.h"
#include "riemann.h"

class Cell;
class Riemann;

class Solve
{
public:
	double FastWaveSpeed(Cell cL, Cell cR);
	double GlobalFastWaveSpeed(Cell*);
	void InitializeConserved(Cell*);
	void CellFromQcons(Cell*, double dt);
	double GlobalTimestep(double vfast){
		// std::cout<<"timestep: "<<cfl*min(dx_global, dy_global)/vfast<<endl;
		return cfl*min(dx_global, dy_global)/vfast;
	};
	void InitializeFlux(double*);
	void UpdateCellValue(Cell*, double);
	void UpdateDQ(Cell&, Cell&, double*, double, double);
	void BoundaryCheck(Cell*, double);
	void UpdateQcons(Cell*);
};


#endif