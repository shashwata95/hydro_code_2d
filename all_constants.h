#ifndef ALLCONSTANTS_H
#define ALLCONSTANTS_H
#include <iostream>
#include <string>

static const int Nx = 500;
static const int Ny = 500;
static const int ncells = Nx*Ny;
static const double tfinal = .6;
static const double dt_snap = .1;
static const double gamma1 = 1.4;
static const double xmin = -10.; //a: domain left edge
static const double xmax = 10.; //b: domain right edge
static const double ymin = -10.;
static const double ymax = 10.;
static const double cfl = 0.1;

static const int ndim = 2;
static const int nvar = ndim + 2; //index order: velocity->0,1,2 density->3, energy->4
static const int irho = ndim;
static const int iE = ndim + 1;
static const std::string ic = "adsod";
static const double total_length = xmax - xmin;
static const double total_length_y = ymax - ymin;
static const double dx_global = total_length/Nx;
static const double dy_global = total_length_y/Nx;

static const int big_number = 99999;
#endif
