#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <assert.h>
#include <iomanip>

#include "initialize.h"
#include "all_constants.h"
#include "cell.h"


void::Initialize::WriteToFile(Cell* cell, int i, double time)
{
	ofstream writefile;
	string name;
	stringstream ss;
	ss<< setw(5)<<setfill('0')<<i;
	string s = ss.str();
	name = "./results/sedov500_" + s +".dat";
	writefile.open(name);
	writefile<<time<<endl;
	writefile<<ncells<<endl;
	for (int cl = 0; cl < ncells; cl++){
		for (int kk = 0; kk < ndim; kk++) writefile<<cell[cl].pos[kk]<<"	";
		for (int kk = 0; kk < ndim; kk++) writefile<<cell[cl].vel[kk]<<"	";
		writefile<<cell[cl].rho<<"	"<<cell[cl].press<<"	";
		writefile<<endl;
	}
	cout<<"file number: "<<i<<endl;
	writefile.close();

}

void Initialize::Adsod(Cell *cell, double rhoL, double rhoR, double PL, double PR)
{
	int border = Nx/2;
	double h = dx_global;
	double ekin;
	double min_len = -total_length/2.0;
	int id;
	for (int i=0;i<Nx;i++) {
		cell[i].boundary_cell = true;
		cell[(Ny-1)*Nx+i].boundary_cell = true;
	}
	for (int j=0;j<Ny;j++) {
		cell[j*Nx].boundary_cell = true;
		cell[j*Nx + Nx-1].boundary_cell = true;
	}

	// cell[0].boundary_cell = true;
	// cell[ncells-1].boundary_cell = true;
	for (int i = 0; i < border; i++){ //left side of shock
		for (int j=0; j < Ny; j++){
			id = j*Nx + i;
			cell[id].volume = h;
			cell[id].rho = rhoL;
			for (int kk = 0; kk < ndim; kk++) cell[id].pos[kk] = 0.0;
			for (int kk = 0; kk < ndim; kk++) cell[id].vel[kk] = 0.0;
			for (int kk = 0; kk < ndim; kk++) cell[id].Bm[kk] = 0.0;
			cell[id].pos[0] = xmin + dx_global*(i+0.5);
			cell[id].pos[1] = ymin + dy_global*(j+0.5);
			cell[id].press = PL;
			cell[id].inten = cell[id].press/cell[id].rho/(gamma1-1);
			ekin = 0;
			for (int kk = 0; kk < ndim; kk++) ekin += cell[id].vel[kk]*cell[id].vel[kk];
			ekin = 0.5*ekin*cell[id].rho;


			cell[id].etot = ekin + cell[id].inten*cell[id].rho;

			for (int kk=0; kk<ndim; kk++) cell[id].Wprim[kk] = cell[id].vel[kk]; //populating the primitive vector
			cell[id].Wprim[irho] = cell[id].rho;
			cell[id].Wprim[iE] = cell[id].press;
			cell[id].cs = sqrt(gamma1*cell[id].press/cell[id].rho);
			cell[id].initialized = true;
		}
	}

	for (int i = border; i < Nx; i++){ //left side of shock
		for (int j=0; j < Ny; j++){
			id = j*Nx + i;
			cell[id].volume = h;
			cell[id].rho = rhoR;
			for (int kk = 0; kk < ndim; kk++) cell[id].pos[kk] = 0.0;
			for (int kk = 0; kk < ndim; kk++) cell[id].vel[kk] = 0.0;
			for (int kk = 0; kk < ndim; kk++) cell[id].Bm[kk] = 0.0;
			cell[id].pos[0] = xmin + dx_global*(i+0.5);
			cell[id].pos[1] = ymin + dy_global*(j+0.5);
			cell[id].press = PR;
			cell[id].inten = cell[id].press/cell[id].rho/(gamma1-1);
			ekin = 0;
			for (int kk = 0; kk < ndim; kk++) ekin += cell[id].vel[kk]*cell[id].vel[kk];
			ekin = 0.5*ekin*cell[id].rho;


			cell[id].etot = ekin + cell[id].inten*cell[id].rho;

			for (int kk=0; kk<ndim; kk++) cell[id].Wprim[kk] = cell[id].vel[kk]; //populating the primitive vector
			cell[id].Wprim[irho] = cell[id].rho;
			cell[id].Wprim[iE] = cell[id].press;
			cell[id].cs = sqrt(gamma1*cell[id].press/cell[id].rho);
			cell[id].initialized = true;
		}
	}
	for (int cl=0; cl<ncells; cl++) assert(cell[cl].initialized);

}

void Initialize::Sedov(Cell * cell, double energy)
{
	double inner_frac  = 0.1;
	double xmin_in = xmin*inner_frac;
	double xmax_in = xmax*inner_frac;
	std::cout<<"inner part range: "<<xmin_in<<"	"<<xmax_in<<endl;
	for (int i=0; i<ncells; i++){
		double xpos = xmin + dx_global*(i+0.5);
		for (int kk=0; kk<ndim; kk++) cell[i].pos[kk] = 0.;
		for (int kk=0; kk<ndim; kk++) cell[i].vel[kk] = 0.;
		cell[i].pos[0] = xpos;
		cell[i].rho = 1.;
		if (xpos > xmin_in and xpos < xmax_in){
			cell[i].press = 10.;
		}	else {
			cell[i].press = 1.;
		}
		cell[i].inten = cell[i].press/cell[i].rho/(gamma1-1);
		double ekin = 0;
		for (int kk = 0; kk < ndim; kk++) ekin += cell[i].vel[kk]*cell[i].vel[kk];
		ekin = 0.5*ekin*cell[i].rho;

		cell[i].etot = ekin + cell[i].inten*cell[i].rho;

		for (int j=0; j<ndim; j++) cell[i].Wprim[j] = cell[i].vel[j]; //populating the primitive vector
		cell[i].Wprim[irho] = cell[i].rho;
		cell[i].Wprim[iE] = cell[i].press;
		cell[i].cs = sqrt(gamma1*cell[i].press/cell[i].rho);

	}
	return;
}

void Initialize::Sedov2D(Cell * cell, double energy, double radius)
{
for (int i=0; i<Nx; i++){
	for (int j=0; j<Ny; j++){
		int id = j*Nx + i;
		cell[id].rho = 1.;
		for (int kk=0; kk<ndim;kk++) cell[id].pos[kk] = 0.;
		for (int kk=0; kk<ndim;kk++) cell[id].vel[kk] = 0.;
		cell[id].pos[0] = xmin + (i + 0.5)*dx_global;
		cell[id].pos[1] = ymin + (j + 0.5)*dy_global;
		double pos = sqrt(cell[id].pos[0]*cell[id].pos[0] + cell[id].pos[1]*cell[id].pos[1]);
		if (pos > radius){
			cell[id].press = 1.;
		} else if ( pos <= radius){
			cell[id].press = 100.;
		}
		// std::cout<<i<<"	"<<j<<"	"<<cell[id].press<<endl;
		cell[id].inten = cell[id].press/cell[id].rho/(gamma1 - 1.);
		cell[id].etot = cell[id].inten*cell[id].rho;
		for (int jj=0; jj<ndim; jj++) cell[id].Wprim[jj] = cell[id].vel[jj]; //populating the primitive vector
		cell[id].Wprim[irho] = cell[id].rho;
		cell[id].Wprim[iE] = cell[id].press;
		cell[id].cs = sqrt(gamma1*cell[id].press/cell[id].rho);

	}
}

	return;

}
