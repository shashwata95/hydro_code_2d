#ifndef CELL_H
#define CELL_H

#include <iostream>

#include "all_constants.h"
using namespace std;

class Cell
{
public:

	// ~Cell();
	int id;
	int idx;
	int idy;
	double pos[ndim];
	double vel[ndim];
	double rho;
	double etot; //Energy per unit volume
	double Bm[ndim];
	double volume;

	double Qcons[nvar];
	double dQ[nvar];
	double Wprim[nvar];

	double inten; //Energy per unit mass
	double press;
	double dx;
	double vface[ndim];
	double cs;
	double FluxCen[nvar];
	bool boundary_cell = false;
	bool initialized = false;
	// double PressFromEtot() {

	// }


	
};
#endif
